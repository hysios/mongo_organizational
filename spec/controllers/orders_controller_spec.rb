require 'spec_helper'

describe OrdersController do
  before(:each) do
    session[:current_user] = User.create(:name => 'hysios')
    
  end

	it "create a order" do
    lambda do 
      post :create, :order => { :total => 0 }
    end.should change(Order, :count).by(1)

    assigns(:order).total.should eq(0)
  end

  describe "switch organizational" do
    before :each do 
      @microsoft = stub_model(Company, :id => 5, :name => 'microsoft')
      @apple     = stub_model(Company, :id => 6, :name => 'apple')
      @google    = stub_model(Company, :id => 7, :name => 'google')
      @facebook  = stub_model(Company, :id => 8, :name => 'facebook')
      @amazon    = stub_model(Company, :id => 8, :name => 'facebook')
    end

    def should_increase_order_with(company, &block)
      session[:current_company] = company
      block.should change(Order.with(database: company.name + '_' + Rails.env), :count).by(1)
    end

    it "switch a organizational" do
     
      should_increase_order_with(@apple) do
        post :create, :order => { :total => 0 }
      end

      should_increase_order_with(@microsoft) do
        post :create, :order => { :total => 0 }
      end

      should_increase_order_with(@google) do
        post :create, :order => { :total => 0 }
      end 

      should_increase_order_with(@facebook) do
        post :create, :order => { :total => 0 }
      end

      should_increase_order_with(@amazon) do
        post :create, :order => { :total => 0 }
      end      

      # mongo_session = assigns(:order).mongo_session
      # database_name = mongo_session.options[:database]
      # database_name.should match(@microsoft.name)


    end


  end
end
