module ApplicationHelper


  def table(models)

    # context = TemplateContext.new(self)
    # context.define(:header) do 

    # end

    # rows = Proc.new do |models|

    #   models.each do |model|
    #     "<tr>"  + 
    #       model.attrbutes.map {|key,value| "<td>#{value}</td>" } +
    #     "</tr>"
    #   end
    # end

    yield Table.new(models, self) if block_given?
    nil
    # render
  end

  class TemplateContext

  end

  class Table
    def initialize(models, context)
      @models = models
      @context = context
    end

    def header
      @context.safe_concat("<table border=\"1\">")
      nil
    end

    def rows(&block)
      @models.map do |model|
        block.call(Row.new(model))
      end 
      nil
    end

    def footer
      @context.safe_concat("</table>")
      nil
    end

  end

  class Column
  end

  class Row
    def initialize(model)
      @item = model
    end

    def item
      @item
    end

    def [](value)
      @item[value]
    end
  end

end
