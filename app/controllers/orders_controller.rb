class OrdersController < ApplicationController
  # before_filter :authenticate_user!
  protect_from_forgery :except => :create 
  before_filter :switch_organizational!
  
  respond_to :json

  def create
    @order = Order.create(params[:order])
    puts Thread.current.object_id
    respond_with @order
  end

end
