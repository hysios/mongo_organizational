class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user, :authenticate_user!
  helper_method :current_company, :switch_organizational!

  def current_user
    session[:current_user]
  end

  def current_company
    session[:current_company]
  end

  def authenticate_user!
    if current_user.nil?
      redirect_to "/users/signup"      
    end
    false
  end

  def switch_organizational!
    return false if check_organization

    if current_company.nil?
      redirect_to "/company/choose"
    else
      Mongoid::Organizational::Sessions.switch_organization(current_company.name)
    end
    false
  end

  def check_organization
    organization = params[:organization]
    if organization
      Mongoid::Organizational::Sessions.switch_organization(organization)
    end
  end

end
