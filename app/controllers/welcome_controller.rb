class WelcomeController < ApplicationController
  # before_filter :authenticate_user!
  before_filter :switch_organizational!

  def index
    @orders = Order.all.to_a
  end
end
