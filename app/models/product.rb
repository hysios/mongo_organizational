class Product
  include Mongoid::Document
  include Mongoid::Organizational
  
  field :name, type: String
  field :price, type: BigDecimal
end
