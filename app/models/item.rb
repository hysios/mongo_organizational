class Item 
  include Mongoid::Document
  include Mongoid::Organizational

  field :amount, type: BigDecimal
end
