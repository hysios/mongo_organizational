# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



Mongoid::Organizational::Sessions.switch_organization(:apple)

Order.create(:total => 1000)
Order.create(:total => 2000)
Order.create(:total => 3000)

Mongoid::Organizational::Sessions.switch_organization(:microsoft)

Order.create(:total => 1111)
Order.create(:total => 2222)
Order.create(:total => 3333)
